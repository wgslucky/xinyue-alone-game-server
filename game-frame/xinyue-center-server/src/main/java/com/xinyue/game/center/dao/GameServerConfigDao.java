package com.xinyue.game.center.dao;

import com.xinyue.game.center.dao.entity.AccountEntity;
import com.xinyue.game.center.dao.entity.GameServerConfigEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author 王广帅
 * @since 2021/11/21 21:55
 */
public interface GameServerConfigDao extends MongoRepository<GameServerConfigEntity, String> {

    
}
