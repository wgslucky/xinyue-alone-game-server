package com.xinyue.game.center.business.account.param;

import lombok.Data;

/**
 * @Author 王广帅
 * @Date 2021/5/23 21:00
 */
@Data
public class AccountLoginRequest {
    private String username;
    private String password;
}
