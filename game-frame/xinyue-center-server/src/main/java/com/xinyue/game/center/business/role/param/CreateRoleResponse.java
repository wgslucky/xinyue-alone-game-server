package com.xinyue.game.center.business.role.param;

import lombok.Data;

/**
 * @author 王广帅
 * @since 2022/1/16 14:45
 */
@Data
public class CreateRoleResponse {
    private String playerId;
    private String nickname;
    private long createTime;
    private long lastLoginTime;

    // 创建角色的token
    private String token;
}
