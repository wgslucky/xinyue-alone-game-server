package com.xinyue.game.center.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Author 王广帅
 * @Date 2021/5/10 23:06
 */
@RestControllerAdvice
public class GlobalExceptionHandler {
    private Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(value = Throwable.class)
    @ResponseBody
    public Object errorHandler(Throwable e) {
        if (e instanceof ServerErrorException) {
            ServerErrorException serverErrorException = (ServerErrorException) e;
            ServerErrorCode errorCode = serverErrorException.getErrorCode();
            ServerResponseEntity serverResponseEntity = new ServerResponseEntity(errorCode);
            logger.error("服务器验证未通过", e);
            return serverResponseEntity;
        } else {
            logger.error("服务器内部异常", e);
            ServerResponseEntity serverResponseEntity = new ServerResponseEntity(-1, "服务器未知异常," + e.getMessage());
            return serverResponseEntity;
        }

    }
}
