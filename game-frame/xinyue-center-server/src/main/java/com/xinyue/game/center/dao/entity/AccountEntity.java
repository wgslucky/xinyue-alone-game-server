package com.xinyue.game.center.dao.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author 王广帅
 * @Date 2021/4/29 0:18
 */
@Document("Account")
@Data
public class AccountEntity {
    @Id
    @Indexed
    private String id;
    @Indexed
    private String username;
    private String password;
    private long registerTime;
    private long lastLoginTime;
    private String loginIp;
    private String registerIp;
    //存储每个区角色的基本信息,key是zoneId
    private Map<String, ZoneRoleEntity> sectionPlayerMap = new HashMap<>();


}
