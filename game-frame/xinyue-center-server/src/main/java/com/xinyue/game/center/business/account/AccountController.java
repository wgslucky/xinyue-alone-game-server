package com.xinyue.game.center.business.account;

import com.xinyue.game.center.business.account.logic.AccountRegisterService;
import com.xinyue.game.center.business.account.param.AccountLoginRequest;
import com.xinyue.game.center.business.account.param.AccountLoginResponse;
import com.xinyue.game.center.business.account.param.AccountRegisterRequest;
import com.xinyue.game.center.common.AbstractGameCenterServerController;
import com.xinyue.game.center.dao.entity.AccountEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author 王广帅
 * @Date 2021/4/29 1:05
 */
@RestController
@RequestMapping("account")
public class AccountController extends AbstractGameCenterServerController {

    @Autowired
    private AccountRegisterService accountRegisterService;
    @Autowired
    private AccountService accountLoginService;

    @PostMapping("register")
    public Object registerAccount(@RequestBody AccountRegisterRequest param) throws Exception {
        AccountEntity account = new AccountEntity();
        account.setUsername(param.getUsername());
        account.setPassword(param.getPassword());
        accountRegisterService.accountRegister(account);
        AccountLoginResponse voLoginSuccess = accountLoginService.getLoginResponse(account);
        return responseSuccess(voLoginSuccess);
    }


    @PostMapping("login")
    public Object loginAccount(@RequestBody AccountLoginRequest loginParam) throws Exception {
        AccountEntity account = accountLoginService.login(loginParam);
        AccountLoginResponse loginSuccess = accountLoginService.getLoginResponse(account);
        return responseSuccess(loginSuccess);
    }


}
