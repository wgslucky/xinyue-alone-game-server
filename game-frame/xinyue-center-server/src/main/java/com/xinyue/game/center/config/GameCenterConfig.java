package com.xinyue.game.center.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 游戏服务器分区配置
 *
 * @Author 王广帅
 * @Date 2021/5/27 22:54
 */
@Configuration
@ConfigurationProperties(prefix = "xinyue.center.config")
@Data
public class GameCenterConfig {
    /**
     * json数据配置路径
     */
    private String jsonDataConfigPath;
    /**
     * token加密密钥
     */
    private String tokenAesKey = "123456AAbbcc#@xm";
    /**
     * 一个token的有效天数
     */
    private int tokenPeriodDay = 30;
    /**
     * 创建角色token的有效期，单位是秒
     */
    private int createRoleTokenPeriodTimeSecond = 15;

}
