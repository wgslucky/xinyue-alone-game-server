package com.xinyue.game.center.common;

/**
 * @Author 王广帅
 * @Date 2021/5/10 23:25
 */
public abstract class AbstractGameCenterServerController {

    protected ServerResponseEntity responseSuccess(Object value) {
        ServerResponseEntity serverResponseEntity = new ServerResponseEntity(200, value);
        return serverResponseEntity;
    }

    protected ServerResponseEntity responseError(ServerErrorCode errorCode) {
        ServerResponseEntity serverResponseEntity = new ServerResponseEntity(errorCode);
        return serverResponseEntity;
    }
}
