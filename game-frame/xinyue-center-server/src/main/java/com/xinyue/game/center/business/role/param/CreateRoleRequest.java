package com.xinyue.game.center.business.role.param;

import lombok.Data;

/**
 * @author 王广帅
 * @since 2022/1/16 14:47
 */
@Data
public class CreateRoleRequest {
    /**
     * 用户id
     */
    private String userId;
    /**
     * 角色昵称
     */
    private String nickname;
    /**
     * 选择的分区id
     */
    private String zoneId;
}
