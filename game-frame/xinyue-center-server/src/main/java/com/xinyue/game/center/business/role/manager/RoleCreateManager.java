package com.xinyue.game.center.business.role.manager;

import com.xinyue.game.center.business.role.param.CreateRoleRequest;
import com.xinyue.game.center.common.ServerErrorCode;
import com.xinyue.game.center.dao.DaoPlayerRepository;
import com.xinyue.game.center.dao.entity.AccountEntity;
import com.xinyue.game.center.dao.entity.PlayerEntity;
import com.xinyue.game.center.dao.entity.ZoneRoleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 王广帅
 * @since 2022/1/16 14:51
 */
@Service
public class RoleCreateManager {
    @Autowired
    private RoleIdManager roleIdManager;
    @Autowired
    private DaoPlayerRepository roleDao;

    /**
     * 检测角色是否已存在
     *
     * @param zoneId
     */
    public void checkRoleExist(AccountEntity accountEntity, String zoneId) {
        if (accountEntity.getSectionPlayerMap().containsKey(zoneId)) {
            throw ServerErrorCode.ZONE_ROLE_EXIST.exception();
        }
    }

    /**
     * 检测角色名字是否已被使用
     *
     * @param nickName
     */
    public void checkNicknameExist(String nickName) {

    }

    /**
     * 检测名字的敏感性
     *
     * @param nickname
     */
    public void checkNicknameSensitive(String nickname) {

    }

    public PlayerEntity createRole(CreateRoleRequest param) {
        PlayerEntity roleEntity = new PlayerEntity();
        String roleId = roleIdManager.generateRoleId(param.getZoneId());
        roleEntity.setPlayerId(roleId);
        roleEntity.setZoneId(param.getZoneId());
        roleEntity.setNickname(param.getNickname());
        roleEntity.setUserId(param.getUserId());
        roleEntity.setCreateTime(System.currentTimeMillis());
        roleDao.save(roleEntity);
        return roleEntity;
    }

    public ZoneRoleEntity getZoneRole(PlayerEntity param) {
        ZoneRoleEntity zoneRoleEntity = new ZoneRoleEntity();
        zoneRoleEntity.setPlayerId(param.getPlayerId());
        zoneRoleEntity.setNickname(param.getNickname());
        long nowTime = System.currentTimeMillis();
        zoneRoleEntity.setCreateTime(nowTime);
        zoneRoleEntity.setLastLoginTime(nowTime);
        return zoneRoleEntity;
    }
}
