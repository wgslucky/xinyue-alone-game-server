package com.xinyue.game.center.business.role;

import com.xinyue.game.center.business.role.param.CreateRoleRequest;
import com.xinyue.game.center.business.role.param.CreateRoleResponse;
import com.xinyue.game.center.common.AbstractGameCenterServerController;
import com.xinyue.game.center.common.ServerResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("role")
public class RoleController extends AbstractGameCenterServerController {

    @Autowired
    private RoleService roleService;

    @PostMapping("createRole")
    public ServerResponseEntity createNewRole(@RequestBody CreateRoleRequest createRoleParam) throws Exception {
        CreateRoleResponse voCreateRole = roleService.createRole(createRoleParam);
        return responseSuccess(voCreateRole);
    }
}
