package com.xinyue.game.center.common;

import com.alibaba.fastjson.JSON;

/**
 * @Author 王广帅
 * @Date 2021/5/10 23:11
 */
public class ServerResponseEntity {
    private int code;
    private String data;

    public ServerResponseEntity() {
    }

    public ServerResponseEntity(int code, Object data) {
        this.code = code;
        this.data = JSON.toJSONString(data);
    }

    public ServerResponseEntity(ServerErrorCode errorCode) {
        this.code = errorCode.getErrorCode();
        this.data = errorCode.getErrorMsg();
    }

    public int getCode() {
        return code;
    }

    public String getData() {
        return data;
    }
}
