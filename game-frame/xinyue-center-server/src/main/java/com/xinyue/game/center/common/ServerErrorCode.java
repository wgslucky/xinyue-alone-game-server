package com.xinyue.game.center.common;

import org.slf4j.helpers.MessageFormatter;

/**
 * 错误码枚举，每一个错误码尽量都单独定义，这样如果游戏将来做国际化，让客户端根据错误码转换错误文字即可。
 *
 * @Author 王广帅
 * @Date 2021/4/29 0:36
 */
public enum ServerErrorCode {
    ACCOUNT_1001(1001, "账号名不能为空"),
    ACCOUNT_1002(1002, "账号长度必须在4~64个字之间，可以是中文"),
    ACCOUNT_1003(1003, "密码不能为空"),
    ACCOUNT_1004(1004, "密码长度必须在4~64位之间"),
    ACCOUNT_1005(1005, "此用户名已注册过"),
    ACCOUNT_1006(1006, "此账号不存在"),
    ACCOUNT_10087(1007, "密码不正确"),
    ACCOUNT_USER_ID_ERROR(1008, "用户ID不合法"),
    ZONE_ROLE_EXIST(1009, "当前分区角色已存在"),
    ;
    private int errorCode;
    private String errorMsg;

    ServerErrorCode(int errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public ServerErrorException exception() {
        ServerErrorException serverErrorException = new ServerErrorException(this, this.toString());
        return serverErrorException;
    }

    public ServerErrorException exception(String format, Object... args) {
        String errorMsg = "";
        if (args == null || args.length == 0) {
            errorMsg = format;
        } else {
            errorMsg = MessageFormatter.arrayFormat(format, args).getMessage();
        }
        return new ServerErrorException(this, errorMsg);
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    @Override
    public String toString() {
        return "ErrorCode{" +
                "errorCode=" + errorCode +
                ", errorMsg='" + errorMsg + '\'' +
                '}';
    }
}
