package com.xinyue.game.center.dao.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 当前对象中存储了用户操作非常频繁的数据，其它的非频繁操作数据，建议走独立的表
 */
@Data
@Document("Player")
public class PlayerEntity {
    /**
     * 用户的唯一id
     */
    @Id
    @Indexed
    private String playerId;
    private String userId;
    private String nickname;
    private String zoneId;
    private int exp;
    private int level = 1;
    private long createTime;


}
