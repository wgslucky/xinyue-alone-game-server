package com.xinyue.game.center.business.account.logic;

import com.xinyue.game.center.common.ServerErrorCode;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * @Author 王广帅
 * @Date 2021/4/29 0:34
 */
@Service
public class AccountManager {

    public void checkUsername(String username) {
        if (StringUtils.isBlank(username)) {
            throw ServerErrorCode.ACCOUNT_1001.exception();
        }
        int size = username.length();
        if (size < 4 || size > 64) {
            throw ServerErrorCode.ACCOUNT_1002.exception();
        }
    }

    public void checkPassword(String password) {
        if (StringUtils.isBlank(password)) {
            throw ServerErrorCode.ACCOUNT_1003.exception();
        }
        int size = password.length();
        if (size < 4 || size > 64) {
            throw ServerErrorCode.ACCOUNT_1004.exception();
        }
    }

    public void checkUserId(String userId) {
        if (StringUtils.isEmpty(userId)) {
            throw ServerErrorCode.ACCOUNT_USER_ID_ERROR.exception("用户id不能为空");
        }
        if (userId.length() > 64) {
            throw ServerErrorCode.ACCOUNT_USER_ID_ERROR.exception("用户id太长了");
        }
    }


}
