package com.xinyue.game.center.gameserver;

import com.xinyue.game.center.business.role.RoleService;
import com.xinyue.game.center.common.AbstractGameCenterServerController;
import com.xinyue.game.center.dataconfig.GameServerInfo;
import com.xinyue.game.dataconfig.GameDataConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

/**
 * @Author 王广帅
 * @Date 2021/5/28 22:54
 */
@RestController
@RequestMapping("gameserver")
public class GameServerController extends AbstractGameCenterServerController {
    @Autowired
    private GameDataConfigService gameDataConfigService;
    @Autowired
    private RoleService roleService;

    @PostMapping("getServerList")
    public Object getGameServerList() {
        Collection<GameServerInfo> gameServerInfoList = gameDataConfigService.getAllDataConfigList(GameServerInfo.class);
        return responseSuccess(gameServerInfoList);
    }

}
