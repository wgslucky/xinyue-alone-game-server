package com.xinyue.game.center.business.role.manager;

import com.xinyue.game.center.common.EnumGameRedisKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

/**
 * @author 王广帅
 * @since 2022/1/16 15:36
 */
@Service
public class RoleIdManager {

    @Autowired
    private StringRedisTemplate redisTemplate;

    public String generateRoleId(String zoneId) {
        String key = getKey(zoneId);
        long roleId = redisTemplate.opsForValue().increment(key);
        return zoneId + "_" + roleId;
    }

    private String getKey(String zoneId) {
        return EnumGameRedisKey.ROLE_ID_KEY.getKey(zoneId);
    }

}
