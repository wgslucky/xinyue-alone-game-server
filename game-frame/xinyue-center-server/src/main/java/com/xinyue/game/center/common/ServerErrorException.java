package com.xinyue.game.center.common;

/**
 * @Author 王广帅
 * @Date 2021/4/29 0:39
 */
public class ServerErrorException extends RuntimeException {
    private ServerErrorCode errorCode;

    public ServerErrorException(ServerErrorCode errorCode, String errorMsg) {
        super(errorMsg);
        this.errorCode = errorCode;
    }

    public ServerErrorCode getErrorCode() {
        return errorCode;
    }
}
