package com.xinyue.game.center;

import com.xinyue.game.center.config.GameCenterConfig;
import com.xinyue.game.dataconfig.GameDataConfigContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author 王广帅
 * @Date 2021/4/29 0:04
 */
@SpringBootApplication
public class GameCenterServerMain implements CommandLineRunner {
    @Autowired
    private GameDataConfigContext gameDataConfigContext;
    @Autowired
    private GameCenterConfig gameCenterConfig;

    public static void main(String[] args) {
        SpringApplication.run(GameCenterServerMain.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        //初始化配置类
        String configClassPck = "com.xinyue.game.center.dataconfig";
        gameDataConfigContext.startReload(gameCenterConfig.getJsonDataConfigPath(), configClassPck);
    }
}
