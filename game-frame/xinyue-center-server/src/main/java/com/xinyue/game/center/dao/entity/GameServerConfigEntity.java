package com.xinyue.game.center.dao.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 * 游戏区配置信息
 * @author 王广帅
 * @since 2021/11/21 21:51
 */
@Data
public class GameServerConfigEntity {
    /**
     *  服务id
     */
    @Id
    private String serverId;
    /**
     * 服务名称
     */
    private String serverName;
    private String host;
    private String port;
    /**
     * 开服时间
     */
    private String startTime;
    private long createTime;
}
