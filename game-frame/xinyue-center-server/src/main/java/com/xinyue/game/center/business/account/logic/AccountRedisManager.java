package com.xinyue.game.center.business.account.logic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.time.Duration;

/**
 * @Author wang guang shuai
 * @Date 2021/4/29 5:25 下午
 * @Desc
 */
@Service
public class AccountRedisManager {
    @Autowired
    private StringRedisTemplate redisTemplate;
    private static final int USERNAME_EXPIRE_HOURS = 1;

    /**
     * <p>
     * 保存用户名，只有当redis中不存在的时候，才更更新成功，如果已存在不做操作，
     * 这样，如果是多个线程同时更新的话，保证只有一个线程更新成功。
     * </p>
     *
     * @param username
     * @return
     */
    public boolean saveUsername(String username) {
        String value = String.valueOf(System.currentTimeMillis());
        return redisTemplate.opsForValue().setIfAbsent(username, value, Duration.ofHours(USERNAME_EXPIRE_HOURS));
    }


}
