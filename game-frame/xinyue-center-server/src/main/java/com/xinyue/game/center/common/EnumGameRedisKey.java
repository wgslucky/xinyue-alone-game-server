package com.xinyue.game.center.common;

/**
 * @author 王广帅
 * @since 2022/1/16 15:37
 */
public enum EnumGameRedisKey {
    ROLE_ID_KEY("role_id_key", "角色id");
    private String key;
    private String desc;

    EnumGameRedisKey(String key, String desc) {
        this.key = key;
        this.desc = desc;
    }

    public String getKey(String zoneId) {
        return zoneId + "_" + this.key;
    }

    public String getKey() {
        return key;
    }

    public String getDesc() {
        return desc;
    }
}
