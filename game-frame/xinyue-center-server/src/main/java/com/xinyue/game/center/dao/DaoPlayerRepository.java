package com.xinyue.game.center.dao;

import com.xinyue.game.center.dao.entity.PlayerEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DaoPlayerRepository extends MongoRepository<PlayerEntity, String> {

}
