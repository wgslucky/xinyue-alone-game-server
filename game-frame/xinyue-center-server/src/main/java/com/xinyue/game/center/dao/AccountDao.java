package com.xinyue.game.center.dao;

import com.xinyue.game.center.dao.entity.AccountEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @Author 王广帅
 * @Date 2021/4/29 0:24
 */
@Repository
public interface AccountDao extends MongoRepository<AccountEntity, String> {

    AccountEntity findByUsername(String username);
}
