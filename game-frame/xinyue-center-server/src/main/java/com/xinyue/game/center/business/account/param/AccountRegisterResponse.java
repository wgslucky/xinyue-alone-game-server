package com.xinyue.game.center.business.account.param;

import lombok.Data;

/**
 * @author 王广帅
 * @date 2022/12/25 00:41
 */
@Data
public class AccountRegisterResponse {
    private String userId;
    private String token;
    
}
