package com.xinyue.game.center.dao.entity;

import lombok.Data;

/**
 * @Author 王广帅
 * @Date 2021/5/23 21:20
 */
@Data
public class ZoneRoleEntity {
    private String playerId;
    private String nickname;
    private long createTime;
    private long lastLoginTime;

}
