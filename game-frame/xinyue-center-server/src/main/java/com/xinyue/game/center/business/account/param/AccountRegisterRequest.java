package com.xinyue.game.center.business.account.param;

import lombok.Data;

/**
 * @Author 王广帅
 * @Date 2021/4/29 1:06
 */
@Data
public class AccountRegisterRequest {
    private String username;
    private String password;
}
