package com.xinyue.game.center.business.account.param;

import com.xinyue.game.center.dao.entity.ZoneRoleEntity;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author 王广帅
 * @Date 2021/5/10 23:22
 */
@Data
public class AccountLoginResponse {
    private String userId;
    private String username;
    private long registerTime;
    private long lastLoginTime;
    private String token;
    //存储每个区角色的基本信息,key是zoneId
    private Map<String, ZoneRoleEntity> sectionPlayerMap = new HashMap<>();

}
