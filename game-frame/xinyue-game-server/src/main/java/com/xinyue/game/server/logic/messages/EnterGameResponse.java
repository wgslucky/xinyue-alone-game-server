package com.xinyue.game.server.logic.messages;

import com.xinyue.game.server.dao.entity.PlayerEntity;
import com.xinyue.server.framework.handlermapping.AbstractGameMessage;
import com.xinyue.server.framework.message.GameMessageMeta;
import lombok.Data;

/**
 * @author 王广帅
 * @since 2022/1/5 21:28
 */
@Data
@GameMessageMeta(messageId = 1001, messageType = 2)
public class EnterGameResponse extends AbstractGameMessage {
    /**
     * 角色信息
     */
    private PlayerEntity role;

}
