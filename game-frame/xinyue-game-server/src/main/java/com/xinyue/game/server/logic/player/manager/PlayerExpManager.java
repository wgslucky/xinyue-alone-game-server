package com.xinyue.game.server.logic.player.manager;

import com.xinyue.game.server.logic.player.event.LoginEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

/**
 * 角色经验管理
 *
 * @author 王广帅
 * @since 2022/2/13 20:40
 */
@Service
public class PlayerExpManager {
    private static final Logger logger = LoggerFactory.getLogger(PlayerExpManager.class);

    @EventListener(LoginEvent.class)
    public void addExp(LoginEvent event) {
        logger.debug("收到用户登陆事件");
    }
}
