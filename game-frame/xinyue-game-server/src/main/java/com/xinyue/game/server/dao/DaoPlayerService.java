package com.xinyue.game.server.dao;

import com.xinyue.game.server.dao.entity.PlayerEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DaoPlayerService implements IDaoPlayerService {
    @Autowired
    private MongoDaoPlayerService mongoDaoPlayerService;

    @Override
    public PlayerEntity getPlayerById(String playerId) {
        return mongoDaoPlayerService.findById(playerId).orElse(null);
    }

    @Override
    public PlayerEntity updatePlayer(PlayerEntity playerEntity) {
        return this.mongoDaoPlayerService.save(playerEntity);
    }

}
