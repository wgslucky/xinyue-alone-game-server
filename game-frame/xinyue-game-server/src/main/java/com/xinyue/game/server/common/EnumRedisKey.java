package com.xinyue.game.server.common;

public enum EnumRedisKey {
    ROLE_ID("game_role_id", "角色ID");
    private String redisKey;
    private String desc;

    EnumRedisKey(String redisKey, String desc) {
        this.redisKey = redisKey;
        this.desc = desc;
    }

    public String getRedisKey() {
        return redisKey;
    }

    public String getDesc() {
        return desc;
    }
}
