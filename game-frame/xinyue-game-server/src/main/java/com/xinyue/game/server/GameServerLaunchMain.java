package com.xinyue.game.server;

import com.xinyue.server.framework.server.GameServerBoot;
import org.springframework.boot.Banner.Mode;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import javax.annotation.PreDestroy;

@SpringBootApplication
public class GameServerLaunchMain {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(GameServerLaunchMain.class);
        app.setBannerMode(Mode.LOG);
        app.setWebApplicationType(WebApplicationType.NONE);
        ApplicationContext context = app.run(args);
        //启动服务，以socket模式与客户端通信
        //GameServerBoot.startSocketServer(context);
        // 启动服务，以websocket的方式与客户端通信
        GameServerBoot.startWebSocketServer(context);
    }

    @PreDestroy
    public void stop() {
        GameServerBoot.stop();
    }
}

