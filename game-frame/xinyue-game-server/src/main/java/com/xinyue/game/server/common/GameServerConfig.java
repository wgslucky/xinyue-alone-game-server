package com.xinyue.game.server.common;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "xinyue.gameserver.config")
public class GameServerConfig {
    /**
     * 游戏异步任务执行的线程数
     */
    private int gameAsyncTaskThreadCount = Runtime.getRuntime().availableProcessors() * 2;
    /**
     * token加密密钥
     */
    private String tokenAesKey = "123456AAbbcc#@xm";
    /**
     * 角色id的起始索引
     */
    private long roleIdStartIndex = 100000;
    /**
     * 更新角色信息延迟时间，单位是秒
     */
    private int updateRoleDelay = 10;
    /**
     * 缓存最大数量
     */
    private int maxCacheSize = 10000;
    private int expireOfAccess = 60;


}
