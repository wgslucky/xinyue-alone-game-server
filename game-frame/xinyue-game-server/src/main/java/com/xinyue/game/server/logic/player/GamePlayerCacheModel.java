package com.xinyue.game.server.logic.player;

import com.xinyue.server.framework.handlermapping.GameCacheModel;
import io.netty.channel.ChannelHandlerContext;

public class GamePlayerCacheModel extends GameCacheModel {
    private String playerId;

    public GamePlayerCacheModel(ChannelHandlerContext nettyCtx, String playerId) {
        super(nettyCtx);
        this.playerId = playerId;
    }

    @Override
    public String getPlayerId() {
        return playerId;
    }
}
