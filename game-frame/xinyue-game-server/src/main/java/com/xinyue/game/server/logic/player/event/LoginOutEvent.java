package com.xinyue.game.server.logic.player.event;

import org.springframework.context.ApplicationEvent;

public class LoginOutEvent extends ApplicationEvent {
    private String playerId;

    public LoginOutEvent(Object source, String playerId) {
        super(source);
        this.playerId = playerId;
    }

    public String getPlayerId() {
        return playerId;
    }
}
