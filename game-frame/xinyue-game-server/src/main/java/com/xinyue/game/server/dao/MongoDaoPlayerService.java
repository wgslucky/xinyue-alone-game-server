package com.xinyue.game.server.dao;

import com.xinyue.game.server.dao.entity.PlayerEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @Author 王广帅
 * @Date 2021/4/28 0:13
 */
public interface MongoDaoPlayerService extends MongoRepository<PlayerEntity, String> {

}
