package com.xinyue.game.server.common;

import com.xinyue.server.framework.common.IGameServerError;

/**
 * @author 王广帅
 * @since 2022/1/5 21:52
 */
public enum EnumGameError implements IGameServerError {
    DEFAULT_ERROR(-1, "服务器异常"),
    TOKEN_ERROR(1001, "token错误"),
    TOKEN_EXPIRE(1002, "token已过期"),
    USER_ID_ERROR(1003, "用户id错误"),
    ROLE_NOT_EXIST(1004, "参数错误，角色不存在");
    private int errorCode;
    private String errorMsg;

    EnumGameError(int errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    @Override
    public int getErrorCode() {
        return this.errorCode;
    }

    @Override
    public String getErrorMsg() {
        return this.errorMsg;
    }
}
