package com.xinyue.game.server.logic.messages;

import com.xinyue.server.framework.handlermapping.AbstractGameMessage;
import com.xinyue.server.framework.message.GameMessageMeta;
import lombok.Data;

/**
 * @author 王广帅
 * @since 2022/1/5 21:27
 */
@Data
@GameMessageMeta(messageId = 1001, messageType = 1, desc = "进入游戏")
public class EnterGameRequest extends AbstractGameMessage {
    private String userId;
    private String zoneId;
    private String playerId;
    private String token;
}
