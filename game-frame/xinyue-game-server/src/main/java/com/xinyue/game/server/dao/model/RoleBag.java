package com.xinyue.game.server.dao.model;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 王广帅
 * @since 2022/2/10 19:49
 */
@Data
public class RoleBag {
    private Map<String, RoleBagItem> roleBagItemMap = new HashMap<>();
}
