package com.xinyue.game.server.logic.player.event;

import com.xinyue.server.framework.handlermapping.GameChannelContext;
import org.springframework.context.ApplicationEvent;

/**
 * @author 王广帅
 * @since 2022/2/13 20:41
 */
public class LoginEvent extends ApplicationEvent {
    private GameChannelContext gtx;

    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public LoginEvent(Object source, GameChannelContext gtx) {
        super(source);
        this.gtx = gtx;
    }

    public GameChannelContext getGtx() {
        return gtx;
    }
}
