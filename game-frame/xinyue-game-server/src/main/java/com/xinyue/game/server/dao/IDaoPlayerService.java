package com.xinyue.game.server.dao;

import com.xinyue.game.server.dao.entity.PlayerEntity;

public interface IDaoPlayerService {
    PlayerEntity getPlayerById(String playerId);

    PlayerEntity updatePlayer(PlayerEntity playerEntity);
}
