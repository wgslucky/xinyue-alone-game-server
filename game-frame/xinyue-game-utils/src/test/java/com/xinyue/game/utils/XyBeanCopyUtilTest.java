package com.xinyue.game.utils;

import com.xinyue.game.utils.model.TestUser;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * @author 王广帅
 * @since 2022/2/13 19:38
 */
public class XyBeanCopyUtilTest {

    @Test
    public void testCopyObj() {
        TestUser testUser = new TestUser();
        String name = "aaa";
        testUser.setName(name);
        String key = "1";
        String value = "北京";
        testUser.getAddressMap().put(key, value);

        TestUser newUser = XyBeanCopyUtil.copyObj(testUser, TestUser.class);
        Assert.assertEquals(newUser.getName(), name);
        Assert.assertEquals(newUser.getAddressMap().get(key), testUser.getAddressMap().get(key));
    }
}