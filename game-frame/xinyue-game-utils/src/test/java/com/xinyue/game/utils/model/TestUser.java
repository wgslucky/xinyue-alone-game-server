package com.xinyue.game.utils.model;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 王广帅
 * @since 2022/2/13 19:39
 */
@Data
public class TestUser {
    private String name;
    private Map<String, String> addressMap = new HashMap<>();
}
