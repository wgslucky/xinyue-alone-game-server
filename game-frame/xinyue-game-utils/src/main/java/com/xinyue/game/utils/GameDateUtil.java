package com.xinyue.game.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class GameDateUtil {

    public static String getStandardDate() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }
}
