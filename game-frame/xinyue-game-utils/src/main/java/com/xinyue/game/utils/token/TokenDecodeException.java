package com.xinyue.game.utils.token;

/**
 * token解码异常
 *
 * @author 王广帅
 * @since 2022/1/8 10:33
 */
public class TokenDecodeException extends Exception {
    public TokenDecodeException(String message) {
        super(message);
    }
}
