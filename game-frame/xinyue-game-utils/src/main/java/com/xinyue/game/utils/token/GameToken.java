package com.xinyue.game.utils.token;

import lombok.Data;

/**
 * @Author 王广帅
 * @Date 2021/5/10 23:50
 */
@Data
public class GameToken extends BaseGameToken {
    //账号id
    private String userId;
    // 选择的区id
    private String zoneId;
    // 创建的角色Id
    private String playerId;

}
