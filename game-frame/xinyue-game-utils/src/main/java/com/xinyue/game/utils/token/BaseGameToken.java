package com.xinyue.game.utils.token;

import lombok.Data;

/**
 * @author 王广帅
 * @since 2022/1/16 18:22
 */
@Data
public class BaseGameToken {
    //token创建时间
    private long createTime;
    //token结束时间
    private long endTime;
}
