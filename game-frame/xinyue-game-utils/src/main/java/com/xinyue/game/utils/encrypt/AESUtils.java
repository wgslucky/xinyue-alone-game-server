package com.xinyue.game.utils.encrypt;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

public class AESUtils {
    private static final String AES = "AES";
    private static final String CIPHER= "AES/ECB/PKCS5Padding";

    public static byte[] encode(byte[] secret, byte[] content) throws Exception {

        SecretKeySpec keySpec = new SecretKeySpec(secret, AES);
        Cipher cipher = Cipher.getInstance(CIPHER);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec);
        byte[] result = cipher.doFinal(content);
        return result;

    }

    public static byte[] decode(byte[] secret, byte[] content) throws Exception {

        SecretKeySpec keySpec = new SecretKeySpec(secret, AES);
        Cipher cipher = Cipher.getInstance(CIPHER);
        cipher.init(Cipher.DECRYPT_MODE, keySpec);
        byte[] result = cipher.doFinal(content);
        return result;

    }

}
