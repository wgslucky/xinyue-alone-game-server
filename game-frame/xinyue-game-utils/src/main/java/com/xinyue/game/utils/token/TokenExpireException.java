package com.xinyue.game.utils.token;

/**
 * token超时异常
 *
 * @author 王广帅
 * @since 2022/1/8 10:33
 */
public class TokenExpireException extends Exception {
    public TokenExpireException(String message) {
        super(message);
    }
}
