package com.xinyue.game.utils;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

/**
 * @author 王广帅
 * @since 2022/2/8 22:37
 */
public class XyBeanCopyUtil {
    private static MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();

    /**
     * 将对象S的数据，复制到对象T中
     *
     * @param data  要复制的数据对象
     * @param clazz 新的接收数据的对象class
     * @param <S>
     * @param <T>
     * @return
     */
    public static <S, T> T copyObj(S data, Class<T> clazz) {
        return mapperFactory.getMapperFacade().map(data, clazz);
    }
}
