package com.xinyue.server.framework.beanconfig;


import com.xinyue.server.framework.common.GameAsyncTaskService;
import com.xinyue.server.framework.common.GameFrameworkConfig;
import com.xinyue.server.framework.handler.GameChannelConfirmHandler;
import com.xinyue.server.framework.handler.GameRequestDispatcherHandler;
import com.xinyue.server.framework.handler.HandShakeHandler;
import com.xinyue.server.framework.handlermapping.GameServerCacheService;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * @author 王广帅
 * @date 2021年01月26日 8:09 下午
 */
@Configuration
@EnableConfigurationProperties(GameFrameworkConfig.class)
public class AutoXinyueGameServerConfiguration {

    @Bean
    public HandShakeHandler handShakeHandler() {
        return new HandShakeHandler();
    }

    @Bean
    public GameAsyncTaskService gameIOTaskService() {
        return new GameAsyncTaskService();
    }

    @Bean
    public GameServerCacheService gameChannelService() {
        return new GameServerCacheService();
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public GameRequestDispatcherHandler gameRequestDispatcherHandler() {
        return new GameRequestDispatcherHandler();
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public GameChannelConfirmHandler channelConfirmHandler() {
        return new GameChannelConfirmHandler();
    }
}
