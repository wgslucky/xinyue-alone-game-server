package com.xinyue.server.framework.handler.message;

import com.xinyue.server.framework.handlermapping.AbstractGameMessage;
import com.xinyue.server.framework.message.GameMessageMeta;

/**
 * @author 王广帅
 * @since 2021/11/13 23:03
 */
@GameMessageMeta(messageId = 2, messageType = 2)
public class UploadAesKeyResponse extends AbstractGameMessage {
    
}
