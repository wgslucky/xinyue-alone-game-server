package com.xinyue.server.framework.handler.message;

import com.xinyue.server.framework.handlermapping.AbstractGameMessage;
import com.xinyue.server.framework.message.GameMessageMeta;

/**
 * @author 王广帅
 * @since 2021/11/13 22:38
 */
@GameMessageMeta(messageId = 1, messageType = 1)
public class GetPublicKeyRequest extends AbstractGameMessage {

}
