package com.xinyue.server.framework.handlermapping;

public abstract class AbstractGameMessage implements IGameMessage {
    /**
     * 是否需要加密，默认是不需要加密消息，如果需要加密当前消息，可以在发送或响应消息时，由子类设置
     */
    private Boolean needEncrypt;
    private Boolean needCompress;


    @Override
    public Boolean checkNeedEncrypt() {
        return this.needEncrypt != null && this.needEncrypt;
    }

    @Override
    public Boolean checkNeedCompress() {
        return this.needCompress != null && this.needCompress;
    }

    public void setNeedEncrypt(Boolean needEncrypt) {
        this.needEncrypt = needEncrypt;
    }

    public void setNeedCompress(Boolean needCompress) {
        this.needCompress = needCompress;
    }
}
