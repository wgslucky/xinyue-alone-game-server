package com.xinyue.server.framework.message;

/**
 * @Author 王广帅
 * @Date 2021/5/30 20:29
 */
public enum InnerMessageIdEnum {
    SHAKE_HANDLER_FIRST_STEP(1),
    SHAKE_HANDLER_SECOND_STEP(2);
    private int messageId;

    InnerMessageIdEnum(int messageId) {
        this.messageId = messageId;
    }

    public int getMessageId() {
        return messageId;
    }
}
