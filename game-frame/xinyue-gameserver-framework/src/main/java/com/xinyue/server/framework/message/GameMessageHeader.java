package com.xinyue.server.framework.message;

import lombok.Data;

/**
 * 游戏消息包头
 *
 * @author 王广帅
 * @since 2021/11/6 23:25
 */
@Data
public class GameMessageHeader {
    /**
     * 消息大小
     */
    private int messageSize;
    /**
     * 请求唯一id
     */
    private int requestId;
    /**
     * 业务消息id
     */
    private int logicMessageId;
    /**
     * 消息创建时间
     */
    private long createTime;
    /**
     * 错误码
     */
    private int errorCode;
    /**
     * 错误信息
     */
    private String errorMsg;
    /**
     * 消息类型
     */
    private GameMessageType messageType;

}
