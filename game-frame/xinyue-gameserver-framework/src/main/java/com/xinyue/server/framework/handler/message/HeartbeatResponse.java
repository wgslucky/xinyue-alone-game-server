package com.xinyue.server.framework.handler.message;

import com.xinyue.server.framework.handlermapping.AbstractGameMessage;
import com.xinyue.server.framework.message.GameMessageMeta;

/**
 * @author 王广帅
 * @since 2022/2/4 18:17
 */
@GameMessageMeta(messageId = 3, messageType = 2)
public class HeartbeatResponse extends AbstractGameMessage {
    
}
