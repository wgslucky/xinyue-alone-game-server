package com.xinyue.server.framework.message;

/**
 * @Author 王广帅
 * @Date 2021/5/30 19:50
 */
public enum GameMessageType {
    REQUEST(1),
    RESPONSE(2),
    PUSH(3);
    private int type;

    GameMessageType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

}

