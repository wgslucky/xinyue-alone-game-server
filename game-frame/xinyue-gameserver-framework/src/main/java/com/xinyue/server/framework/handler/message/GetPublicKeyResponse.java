package com.xinyue.server.framework.handler.message;

import com.xinyue.server.framework.handlermapping.AbstractGameMessage;
import com.xinyue.server.framework.message.GameMessageMeta;

/**
 * @author 王广帅
 * @since 2021/11/13 22:24
 */
@GameMessageMeta(messageType = 2, messageId = 1)
public class GetPublicKeyResponse extends AbstractGameMessage {
    private String publicKey;

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

}
