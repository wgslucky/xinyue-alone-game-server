package com.xinyue.server.framework.common;

public interface IGameServerError {

    int getErrorCode();

    String getErrorMsg();
}
