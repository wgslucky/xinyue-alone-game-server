package com.xinyue.server.framework.common;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "xinyue.framework.config")
@Data
@Configuration
public class GameFrameworkConfig {

    private int port = 8088;
    private int bossThreads = 1;
    /**
     * 工作线程的数量
     **/
    private int workThreads = Runtime.getRuntime().availableProcessors();
    private int sendBuffSize = 65535;
    private int receiveBuffSize = 65535;
    /**
     * 接收到消息的最大长度
     */
    private int maxFrameLength = 1024 * 1024;
    /**
     * 客户端请求QPS限制
     */
    private int requestQps = 50;
    /**
     * 全局客户端请求QPS限制
     */
    private int globalRequestQps = 5000;
    /**
     * 连接最大空闲时间,单位秒
     */
    private int maxIdleTime = 60;
    /**
     * 连接认证超时时间，单位秒
     */
    private int confirmTimeout = 30;
    /**
     * 消息响应的缓存时间,单位毫秒
     */
    private int responseCacheTime = 120000;
    /**
     * 游戏IO异步线程数
     */
    private int ioThreadCount = Runtime.getRuntime().availableProcessors();

    /**
     * 游戏缓存对象的最大个数
     */
    private int maxGameCacheModelCount = 2048;
    /**
     * 游戏缓存对象在内存中的空闲最大时间，在这个时间内如果对象不再被使用，将被移除,单位是秒
     */
    private int maxGameCacheModelIdleTime = 600;
}
