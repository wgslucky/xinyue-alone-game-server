package com.xinyue.server.framework.common;

public enum GameFrameErrorEnum implements IGameServerError {
    READ_MESSAGE_ERROR(-1, "客户端消息读取失败"),
    ;
    private int errorCode;
    private String errorMsg;

    GameFrameErrorEnum(int errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    @Override
    public int getErrorCode() {
        return errorCode;
    }

    @Override
    public String getErrorMsg() {
        return errorMsg;
    }
}
