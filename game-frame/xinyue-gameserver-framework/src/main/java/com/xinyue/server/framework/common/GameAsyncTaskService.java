package com.xinyue.server.framework.common;

import com.xinyue.server.framework.handlermapping.GameChannelContext;
import io.netty.util.concurrent.DefaultEventExecutor;
import io.netty.util.concurrent.EventExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;

/**
 * 游戏中异步执行任务管理类
 *
 * @author 王广帅
 * @since 2022/1/5 21:40
 */
public class GameAsyncTaskService {
    private static final Logger logger = LoggerFactory.getLogger(GameAsyncTaskService.class);
    private EventExecutor[] executors;
    @Autowired
    private GameFrameworkConfig gameServerConfig;

    @PostConstruct
    public void init() {
        int threadCount = gameServerConfig.getIoThreadCount();
        if (threadCount < 1) {
            throw new IllegalArgumentException("游戏异步任务线程数必须大于0");
        }
        executors = new EventExecutor[threadCount];
        for (int i = 0; i < threadCount; i++) {
            final int index = i;
            executors[i] = new DefaultEventExecutor(r -> {
                return new Thread(r, "游戏IO线程-" + index);
            });
        }
    }

    public EventExecutor selectExecutor(String key) {
        if (StringUtils.isEmpty(key)) {
            throw new IllegalArgumentException("执行异步任务的key不能为空");
        }
        int index = Math.abs(key.hashCode() % executors.length);
        EventExecutor executor = this.executors[index];
        return executor;
    }

    public void execute(String key, String taskName, Runnable task) {

        if (StringUtils.isEmpty(taskName)) {
            throw new IllegalArgumentException("执行异步任务的taskName不能为空");
        }
        logger.debug("加入游戏异步执行任务：{}", taskName);
        EventExecutor executor = selectExecutor(key);
        executor.execute(() -> {
            logger.debug("开始异步执行任务: {}", taskName);
            long startTime = System.currentTimeMillis();
            try {
                task.run();
            } catch (Throwable e) {
                logger.error("游戏异步任务异常，任务名：{}", task, e);
            }
            long endTime = System.currentTimeMillis();
            logger.debug("异步执行-{}-任务结束,用时：{} ms", taskName, (endTime - startTime));
        });
    }

    public void execute(GameChannelContext gtx, String key, String taskName, Runnable task) {

        if (StringUtils.isEmpty(taskName)) {
            throw new IllegalArgumentException("执行异步任务的taskName不能为空");
        }
        logger.debug("加入游戏异步执行任务：{}", taskName);
        EventExecutor executor = selectExecutor(key);
        executor.execute(() -> {
            logger.debug("开始异步执行任务: {}", taskName);
            long startTime = System.currentTimeMillis();
            try {
                task.run();
            } catch (Throwable e) {
                logger.error("游戏异步任务异常，任务名：{}", task, e);
                gtx.writeAndFlush(e);
            }
            long endTime = System.currentTimeMillis();
            logger.debug("异步执行-{}-任务结束,用时：{} ms", taskName, (endTime - startTime));
        });
    }
}
