package com.xinyue.server.framework.server;

import lombok.Data;

/**
 * @author 王广帅
 * @date 2023/4/5 18:02
 */
@Data
public class EncodeResult {

    private byte[] bodyBytes;
    private String bodyStr;

}
