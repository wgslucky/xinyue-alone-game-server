package com.xinyue.server.framework.handlermapping;

/**
 * @author 王广帅
 * @since 2021/11/7 0:32
 */
public interface IGameMessage {
    /**
     * 返回消息是否需要加密
     *
     * @return
     */
    Boolean checkNeedEncrypt();

    /**
     * 是否要压缩消息
     *
     * @return
     */
    Boolean checkNeedCompress();
}
