package com.xinyue.server.framework.common;

import org.slf4j.helpers.MessageFormatter;

public class GameLogicException extends RuntimeException {
    private IGameServerError serverError;

    public GameLogicException(IGameServerError serverError) {
        super(serverError.toString());
        this.serverError = serverError;
    }

    public GameLogicException(IGameServerError serverError, String format, Object... params) {
        super(serverError.toString() + "," + MessageFormatter.arrayFormat(format, params).getMessage());
    }

    public IGameServerError getServerError() {
        return serverError;
    }


}
