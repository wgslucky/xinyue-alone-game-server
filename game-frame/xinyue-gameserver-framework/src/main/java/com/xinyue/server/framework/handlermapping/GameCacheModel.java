package com.xinyue.server.framework.handlermapping;

import io.netty.channel.ChannelHandlerContext;
import lombok.Data;

@Data
public abstract class GameCacheModel {
    /**
     * netty的handler处理类的上下文对象
     */
    private ChannelHandlerContext nettyCtx;


    public GameCacheModel(ChannelHandlerContext nettyCtx) {
        this.nettyCtx = nettyCtx;
    }

    /**
     * 获取当前玩家的唯一id，由子类返回，因为只有在用户登陆成功之后才会有这个值
     *
     * @return
     */
    public abstract String getPlayerId();

}
