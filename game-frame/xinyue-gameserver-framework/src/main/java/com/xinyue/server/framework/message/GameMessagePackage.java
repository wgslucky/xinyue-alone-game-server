package com.xinyue.server.framework.message;

import com.xinyue.server.framework.handlermapping.IGameMessage;
import lombok.Data;

/**
 * 客户端与服务器交互的消息包
 *
 * @author 王广帅
 * @since 2021/11/6 23:31
 */
@Data
public class GameMessagePackage {
    /**
     * 消息头
     */
    private GameMessageHeader header;
    /**
     * 消息体
     */
    private IGameMessage body;
}
