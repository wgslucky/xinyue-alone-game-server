# xinyue-alone-game-server



## 摘要

这是一款单服框架的游戏服务器架构。俗话说，麻雀虽小，五脏俱全，虽然是单服框架，但是也是包罗了很多游戏服务器开发必用的技术要点，方便部署和使用，特别是对于有游戏逻辑开发经验，想转行做框架的同志，本项目有很大的参考值价。基本上包括了游戏开发的大多数基本功能。


## 客户端
* unity3d
* Newtonsoft.Json
* 协议-http post
* 异步socket通信
* 异常websocket通信

<hr>

1. 设置服务器地址及长连接方式
   ![输入图片说明](images/c1.png)
   ![输入图片说明](images/c5.png)
2. 登陆界面
   ![输入图片说明](images/c2.png)
3. 选服界面
   ![输入图片说明](images/c3.png)
4. 登陆成功界面
   ![输入图片说明](images/c4.png)
## 服务器端
* Spring Boot   
* Netty Client/Server  实现网络层的客户端与服务器通信
* Logback 日志框架
* MongoDB 数据库
* Redis   缓存
* lombok  用于自动生成类的Getter和Setter方法，在IDEA中需要安装Lombok插件  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0523/210511_b2cc8c37_23677.png "屏幕截图.png")

## 主要功能

* 实现unity C#客户端与服务器的TCP SOCKET网络连接（比较大型的游戏会使用这种，比较端游，APP游戏）
* 实现unity C#Ptyn端与服务器的WEB SOCKET网络连接（小程序游戏会使用这种，比如微信小程序，抖音小程序，或h5小游戏）
* 实现网络连接的断开重连功能，解决在弱网环境下网络不稳定的问题
* 实现网络层的心跳机制
* 实现网络消息的序列化与反序列化
* 实现网络消息包的粘包与断包处理
* 实现账号注册与登陆功能
* 服务器实现角色数据缓存及自动更新功能

## 部署方式
1. 安装mongodb，redis
2. 从服务器端clone到本地
3. 修改xinyue-game-server项目中resource目录中的数据库及redis配置为自己本地的配置
4. 启动项目中unity已打包好的客户端exe程序alone-game-client_v3.0.1.exe

<hr>

欢迎关注下面的公众号，获取更多信息

![公众号，欢迎关注](https://images.gitee.com/uploads/images/2021/0130/014411_4f903920_23677.png "文章中插入的公众号关注.png")

![输入图片说明](images/can.jpg)

<hr>
微信支付获取客户端源码及全部技术服务

![微信支付](images/weixin.png)